FROM openjdk:18.0.1.1-jdk
COPY target/eureka-server-1.0-SNAPSHOT.jar .

CMD [ "java", "-jar", "eureka-server-1.0-SNAPSHOT.jar" ]